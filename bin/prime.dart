import 'package:prime/prime.dart' as prime;
import 'dart:io';

bool isPrime(N) {
  for (var i = 2; i <= N / i; ++i) {
    if (N % i == 0) {
      return false;
    }
  }
  return true;
}

void main() {
  print('Enter Number');
  var Number = int.parse(stdin.readLineSync()!);
  if (isPrime(Number)) {
    print('$Number is a prime number.');
  } else {
    print('$Number is not a prime number.');
  }
}
